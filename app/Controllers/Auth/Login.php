<?php

namespace App\Controllers\Auth;

use App\Controllers\BaseController;

class Login extends BaseController
{

    public function index()
    {

        if (!session()->is_logged) {
            return view('auth/login');    
        }

        // if(session()->)

        return redirect()->route('dashboard');
    }

    public function signin()
    {
        if(!$this->validate([
            'email' =>  'required|valid_email',
            'pwd'   =>  'required'
        ])){
            return redirect()->back()
                ->with('errors', $this->validator->getErrors())
                ->withInput();
        }

        $email = trim($this->request->getVar('email'));
        $password = trim($this->request->getVar('pwd'));

        $model = model('UserModel');

        if(!$user = $model->getUserBy('email',$email))
        {
            return redirect()
                ->back()
                ->with('msg',[
                    'type'=>'danger', 
                    'body' => 'Usuario no registrado correctamente!'
                ]);
        }

        if (!password_verify($password, $user->password)) {
            return redirect()
                ->back()
                ->with('msg',[
                    'type'=>'danger', 
                    'body' => 'Credenciales invalidas!'
                ]);
        }

        session()->set([
            'id_user' => $user->id,
            'username' => $user->username,
            'user_group' => $user->getRole(),
            'is_logged' => true
        ]);

        return redirect()->route('home')->with('msg',[
            'type'=>'success', 
            'body' => 'Vienvenido nuevamente! ' . $user->username
        ]);

    }

    public function signout()
    {
        session()->destroy();
        return redirect()->route('login');
    }
}

