<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index(): string
    {
        $pModel = model('ProductsModel');
        $productos = $pModel->whereIn('id', [10,12,5,4, 16,18])->findAll();
        $destacados = $pModel->whereIn('id', [13,15,17, 16,18])->findAll();
        return view('index',[
            'productos' => $productos,
            'destacados' => $destacados
        ]);
        
    }

    public function nosotros():string
    {
        return view('nosotros');
    }

    public function contacto():string
    {
        return view('contacto');
    }

    public function politicasPrivacidad():string
    {
        return view('politicas_empresa');
    }

    public function comercializacion():string
    {
        return view('comercializacion');
    }

    public function terminos():string
    {
        return view('terminos_condiciones');
    }
}

