<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Entities\Product;

class ProductosController extends BaseController
{
    public function index(): string
    {
        $model = model('ProductsModel');

        return view('admin/productos/list',[

            'products' => $model->paginate(config('BlogConfig')->regPerPage),
            'pager' => $model->pager

        ]);
        
        
    }


    public function create()
    {
        $cModel = model('CategoriesModel');
        return view('admin/productos/create', [
            'categories' => $cModel->findAll()
        ]);
    }

    public function store()
    {
        // dd($this->request->getPost(), $this->request->getFile('cover'));
        if (!$this->validate([
            
            'name' =>  'required|max_length[120]',
            'description' =>  'required',
            'categories.*' => 'permit_empty|is_not_unique[categories.id]',
            'precio'    =>  'required',
            'precio_vta'    => 'required',
            // 'file' =>   'uploaded[cover]|is_image[file]'
        ])) {
            
            return redirect()->back()->withInput()->with('msg',[
                'type'  => 'danger',
                'body' =>   'tienes campos incorrectos'
            ])->with('errors', $this->validator->getErrors());
        }
        
        $file = $this->request->getFile('file');
        // dd($file);
        $product = new Product($this->request->getPost());
        $product->slug = $this->request->getVar('name');
        $product->file = $file->getRandomName();

        if (!$file->hasMoved()) {
            $ruta = ROOTPATH . 'public/uploads/imgs';
            $file->move($ruta, $product->file);
        }
        

        $pModel = model('ProductsModel');

        $pModel->assignCategories($this->request->getVar('categories'));
        $pModel->insert($product);

        return redirect('productos')->with('msg', [
            'type' => 'success',
            'body' => 'La categoria fue guardada correctamente'
        ]);
    }

    public function edit(string $id)
    {
        
        $pModel = model("ProductsModel");
        
        if(!$product = $pModel->find($id))
        {
            dd('no encontro el producto');
        }
        
        $categories = $product->getCategorias();
        
        return view('admin/productos/edit', [
            'product' => $product,
            'categories' => $categories
        ]);
    }

    public function update()
    {
        // dd($this->request->getFile('file')->isFile());
        if (!$this->validate([
            'id' =>  'required|is_not_unique[products.id]',
            'name' =>  'required|max_length[120]',
            'description' =>  'required',
            'precio' => 'required',
            'precio_vta' => 'required',
            // 'categories.*' => 'permit_empty|is_not_unique[categories.id]',
        ])) {
            return redirect()->back()->withInput()->with('msg',[
                'type'  => 'danger',
                'body' =>   'tienes campos incorrectos'
            ])->with('errors', $this->validator->getErrors());
        }
        // dd($this->request->getPost());
        $model = model('ProductsModel');
        
        $file = $this->request->getFile('file');
        
        if($file->isFile())
        {
            $randomFile= $file->getRandomName();
            
            if (!$file->hasMoved()) {
                $ruta = ROOTPATH . 'public/uploads/imgs';
                $file->move($ruta, $randomFile);
            }
        }else{
            $producto = $model->find($this->request->getVar('id'));
            $randomFile = $producto->file;
        }



        $model->save([
            'id' => $this->request->getVar('id'),
            'name' => trim($this->request->getVar('name')),
            'description' => trim($this->request->getVar('description')),
            'categories' => $this->request->getVar('categories'),
            'file' => $randomFile
        ]);

        return redirect('productos')->with('msg', [
            'type' => 'success',
            'body' => 'La categoria fue actualizada correctamente'
        ]);
    }

    public function ver($id)
    {
        
        $pModel = model('ProductsModel');
        
        if(!$producto = $pModel->find($id))
        {
            dd('no encontro el producto');
        }

        // dd($producto, $producto->getCategorias());

        return view('admin/productos/show',[
            'producto' => $producto,
            'categorias' => $producto->getCategorias()
        ]);
        
    }

    public function delete($id)
    {
        $pModel = model('ProductsModel');
        if(!$producto = $pModel->find($id))
        {
            dd('no encontro el producto');
        }

        $pModel->delete($id);
        
        return redirect('productos')->with('msg', [
            'type' => 'success',
            'body' => 'El prodcuto fue eliminada correctamente'
        ]);
                
    }
}
