<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

class UsuariosController extends BaseController
{
    public function index(): string
    {
        $model = model('UserModel');

        return view('admin/usuarios/list',[

            'users' => $model->paginate(config('BlogConfig')->regPerPage),
            'pager' => $model->pager

        ]);
        
        
    }
}
