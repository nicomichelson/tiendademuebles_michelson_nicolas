<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Entities\Product;

class VentasController extends BaseController
{

    public function index()
    {
        $model = model('VentasCabeceraModels');
        

        return view('admin/ventas/list',[

            'ventas' => $model->paginate(config('BlogConfig')->regPerPage),
            'pager' => $model->pager

        ]);
    }

    private function getProductos($detalles)
    {
        $arr=[];
        foreach ($detalles  as $v) {
            array_push($arr, $v->producto_id);
        }
        
        return $arr;
    }

    public function detalle($id)
    {
        $model = model('VentasDetalleModel');
        $pModel = model('ProductsModel');
        
        if (!$detalles = $model->where('venta_id', $id)->findAll() ) {
            dd("no hay de talles para esta venta");
        }

        
        $prodIds = $this->getProductos($detalles);
        
        
        //obtener todos los productos de una venta

        return view('admin/ventas/detalle',[

            'products' => $pModel->whereIn('id',$prodIds)->paginate(config('BlogConfig')->regPerPage),
            'pager' => $pModel->pager

        ]);
    }


}