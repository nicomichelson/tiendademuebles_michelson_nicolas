<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use CodeIgniter\Exceptions\PageNotFoundException;

class CategoriesController extends BaseController
{
    public function index()
    {
        // class="<?php preg_match('|^admin/categorias(\S)*|', service('request')->uri->getPath(), $matches) ? 'active' : ''"
        
        $model = model('CategoriesModel');
        
        return view('admin/categories/list',[
            'categories' => $model->orderBy('id', 'DESC')->paginate(config('BlogConfig')->regPerPage),
            'pager' => $model->pager
        ]);
    }

    public function create()
    {
        
        return view('admin/categories/create');
    }

    public function store()
    {
        if (!$this->validate([
            'name' =>  'required|max_length[120]'
        ])) {
            return redirect()->back()->withInput()->with('msg',[
                'type'  => 'danger',
                'body' =>   'tienes campos incorrectos'
            ])->with('errors', $this->validator->getErrors());
        }
        
        $model = model('CategoriesModel');
        $model->save([
            'name' => trim($this->request->getVar('name')),
            'description' => trim($this->request->getVar('description')),
        ]);

        return redirect('categories')->with('msg', [
            'type' => 'success',
            'body' => 'La categoria fue guardada correctamente'
        ]);
    }

    public function edit(string $id)
    {
        $model = model("CategoriesModel");

        if(!$category = $model->find($id))
        {
            throw PageNotFoundException::forPageNotFound();
        }

        return view('admin/categories/edit', [
            'category' => $category
        ]);
    }

    public function update()
    {
        dd($this->request->getPost());

        if (!$this->validate([
            'name' =>  'required|max_length[120]',
            'id' =>  'required|is_not_unique[categories.id]'
        ])) {
            return redirect()->back()->withInput()->with('msg',[
                'type'  => 'danger',
                'body' =>   'tienes campos incorrectos'
            ])->with('errors', $this->validator->getErrors());
        }
        
        $model = model('CategoriesModel');
        $model->save([
            'id' => $this->request->getVar('id'),
            'name' => trim($this->request->getVar('name')),
            'description' => trim($this->request->getVar('description')),
        ]);

        return redirect('categories')->with('msg', [
            'type' => 'success',
            'body' => 'La categoria fue actualizada correctamente'
        ]);
    }

    public function delete($id)
    {
        $model = model("CategoriesModel");
        $model->delete($id);

        return redirect('categories')->with('msg', [
            'type' => 'success',
            'body' => 'La categoria fue eliminada correctamente'
        ]);
    }
}
