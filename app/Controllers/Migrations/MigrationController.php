<?php

namespace App\Controllers\Migrations;

use App\Controllers\BaseController;

class MigrationController extends BaseController
{

    public function createMigration()
    {

        $migrate = \Config\Services::migrations();

        try {
            $migrate->latest();
            // $migrate->regress(0);
        } catch (\Throwable $th) {
            echo $th;
        }

        
    }

    public function createSeeder()
    {

        $seeder = \Config\Database::seeder();

        try {
            $seeder->call('InitSeeders');
            // $migrate->regress(0);
        } catch (\Throwable $th) {
            echo $th;
        }
        
    }
    
}

