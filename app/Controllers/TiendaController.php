<?php

namespace App\Controllers;


use CodeIgniter\API\ResponseTrait;
use App\Entities\VentasCabeceraEntity;


class TiendaController extends BaseController
{
    use ResponseTrait;
    public function index(): string
    {
        helper('text');
        $products = model('ProductsModel');

        return view('tienda',[
            // 'products' => $products->published()->orderBy('id','DESC')->paginate(config('BlogConfig')->regPerPage),
            'products' => $products->orderBy('id','DESC')->paginate(config('BlogConfig')->regPerPage),
            'pager' => $products->pager

        ]);
    }

    public function producto($id)
    {
        //detalle producto
        $pModel = model('ProductsModel');
        if(!$producto = $pModel->find($id)){
            dd('no existe el producto');
        }
        
        return view('tienda/producto',[
            'producto' => $producto
        ]);
    }

    //clase venta
    public function comprar($id)
    {   
        //formulario compra
        $pModel = model('ProductsModel');
        
        if(!$producto = $pModel->find($id)){
            dd('no existe el producto');
        }

        return view('tienda/comprar',[
            'producto' => $producto
        ]); 
    }

    public function listarCarrito()
    {
        return view('tienda/carrito');
    }

    public function venta()
    {

        if (!session()->is_logged) {
            
            return $this->respond([
                'status' => 'success',
                'message' => 'Debe estar Logeado para poder terminar la compra!!',
                'data' => null,
                'logeado' => false
            ]);   
        }


        if ($this->request->isAJAX() ) {
            // Obtener los datos enviados
            $json = $this->request->getJSON();
            // $carrito = $json->carrito;

            if($carrito = $json->carrito ){
                $carritoData = json_decode($carrito, true);
            
                $cabeceraModel = model('VentasCabeceraModels');
            
                $cabeceraModel->assignVentas($carritoData);
                $t = $this->totalCompra($carritoData);
                $cabeceraModel->insert([
                    'total_venta' => $t['total'] ,
                    'user_id' => session()->id_user,
                    'cantidad' => $t['cantidad']
                    // 'ventas' => $carritoData
                ]);

                $mensaje = 'Su compra se a realizado con Exito!!';
            }
            // Decodificar el JSON si es necesario
            



            // Responder con un JSON
            return $this->respond([
                'status' => 'success',
                'message' => $mensaje?:'Su carrito esta vacio',
                'data' => $carritoData,
                'logeado' => true
            ]);
       
       
        } else {
            // Si no es una solicitud AJAX, responder con un error
            return $this->fail('Invalid request', 400);
        }
        
    }

    private function totalCompra($carrito)
    {
        $arr = [];
        $total = 0;
        $cantidad = 0;
        foreach ($carrito as $v) {
            $total += floatval($v['subtotal']);
            $cantidad += intval($v['cantidad']);
        }

        return $arr[]=[
                'total' => $total,
                'cantidad' => $cantidad
            ];
    }
   
    public function compraUsuario()
    {
        if(!session()->is_logged)
        {
            dd('usuario no logeado');
        }
       
        $model = model('VentasCabeceraModels');
        $ventas = $model->where('user_id', session()->id_user);
        // dd($model->where('user_id', session()->id_user));

        return view('tienda/compraUsuario',[
            'ventas' => $ventas->orderBy('id','DESC')->paginate(config('BlogConfig')->regPerPage),
            'pager' => $ventas->pager
        ]);

    }
}
