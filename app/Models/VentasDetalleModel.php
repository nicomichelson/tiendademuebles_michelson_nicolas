<?php

namespace App\Models;

use CodeIgniter\Model;

class VentasDetalleModel extends Model
{
    protected $table            = 'ventas_detalle';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['venta_id', 'producto_id', 'cantidad', 'precio'];

    // protected bool $allowEmptyInserts = false;
    // protected bool $updateOnlyChanged = true;

   

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    

}