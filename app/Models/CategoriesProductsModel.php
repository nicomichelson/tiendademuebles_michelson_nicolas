<?php

namespace App\Models;

// use App\Entities\Product;
use CodeIgniter\Model;

class CategoriesProductsModel extends Model
{
    protected $table            = 'products_categories';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['category_id', 'product_id'];

    // protected bool $allowEmptyInserts = false;
    // protected bool $updateOnlyChanged = true;

   

    // Dates
    // protected $useTimestamps = false;
    // protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // protected $allowCallbacks = true;
    // protected $afterInsert = ['storageCategories'];

    

}
