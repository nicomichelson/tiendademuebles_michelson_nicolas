<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Entities\User;
use App\Entities\UserInfo;

class UserModel extends Model
{
    protected $table            = 'users';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = User::class;
    protected $useSoftDeletes   = true;

    protected $allowedFields    = ['username','password', 'group', 'email'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $beforeInsert = ['addGroup'];
    protected $afterInsert = ['storeUserInfo'];
    
    protected $assignGroup;
    protected $userInfo;


    protected function addGroup($data)
    {
        $data['data']['group'] = $this->assignGroup;
        return $data;
    }

    public function withGroup(string $group)
    {
        $row = $this->db->table('groups')
                ->where('name', $group)
                ->get()->getFirstRow();
        
        if ($row != null) {
            $this->assignGroup = $row->id;
        }
        
    }


    public function addUserInfo(UserInfo $ui)
    {
        
        $this->userInfo = $ui;

    }

    protected function storeUserInfo($data)
    {
        // dd($this->userInfo);
        $this->userInfo->id_user = $data['id'];

        $model = model('InfoUsersModel');

        $model->insert($this->userInfo);
    }

    public function getUserBy(string $column, string $value)
    {
        return $this->where($column, $value)->first();
    }

    
}
