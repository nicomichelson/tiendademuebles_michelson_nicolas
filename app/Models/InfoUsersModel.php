<?php

namespace App\Models;

use App\Entities\UserInfo;
use CodeIgniter\Model;

class InfoUsersModel extends Model
{
    protected $table            = 'info_users';
    protected $primaryKey       = 'id_user';
    protected $useAutoIncrement = true;
    protected $returnType       = UserInfo::class;
    protected $useSoftDeletes   = false;
    protected $allowedFields    = ['name','surname'];

   

    // Dates
    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';


    public function getFullName()
    {
        return $this->name . " " .$this->suername;
    }

    public function getName()
    {
        $this->attributes['name'];
    }
    
    public function nico()
    {
        return 'hjkahsjkd';
    }

}
