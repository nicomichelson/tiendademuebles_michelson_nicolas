<?php

namespace App\Models;

use App\Entities\Product;
use CodeIgniter\Model;

class ProductsModel extends Model
{
    protected $table            = 'products';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = Product::class;
    protected $useSoftDeletes   = true;
    protected $protectFields    = true;
    protected $allowedFields    = ['name', 'description', 'file', 'slug', 'precio', 'precio_vta', 'stock', 'stock_min'];

    // protected bool $allowEmptyInserts = false;
    // protected bool $updateOnlyChanged = true;

   

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $allowCallbacks = true;
    protected $afterInsert = ['storeCategories'];

    protected $categories = [];

    // public function published()
    // {
    //     $this->where('published_at <=', date('Y-m-d H:i:s'));
    //     return $this;
    // }

    protected function storeCategories(array $data)
    {
        if(!empty($this->categories))
        {
            $cpModel = model('CategoriesProductsModel');

            $cats = [] ;
           
            foreach ($this->categories as $v ) {
                
                $cats[] = [
                    'category_id' => $v,
                    'product_id' => $data['id'],
                ];
            }
            $cpModel->insertBatch($cats);
        }

        return $data;
    }
    
    public function assignCategories(array $categories)
    {
        $this->categories = $categories;
    }

}
