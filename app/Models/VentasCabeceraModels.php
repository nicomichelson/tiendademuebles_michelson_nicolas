<?php

namespace App\Models;

use App\Entities\Product;
use App\Entities\VentasCabeceraEntity;
use CodeIgniter\Model;

class VentasCabeceraModels extends Model
{
    protected $table            = 'ventas_cabecera';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = VentasCabeceraEntity::class;
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['total_venta','user_id','total_venta','cantidad'];

    // protected bool $allowEmptyInserts = false;
    // protected bool $updateOnlyChanged = true;

   protected $ventas = [];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    //callback
    protected $allowCallbacks = true;
    protected $afterInsert = ['storeVentas'];

    protected function storeVentas(array $data)
    {
        // dd('entro', $this->ventas[1]['id']);
        if(!empty($this->ventas)){
            $vDetallesModel= model('VentasDetalleModel');
            $prods = [];
            
            foreach ($this->ventas as $v) {
                $prods[] = [
                    'venta_id' => $data['id'],
                    'producto_id' => $v['id'],
                    'cantidad' => $v['cantidad'],
                    'precio' => $v['precio']
                ];
            }
            
            $vDetallesModel->insertBatch($prods);
        }

        return $data;
    }


    public function assignVentas(array $ventas)
    {
        $this->ventas = $ventas;
    }

   
}