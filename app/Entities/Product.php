<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class Product extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];




    protected function setSlug(string $title)
    {
        $slug = mb_url_title($title, "-");
        $productModel = model('ProductsModel');

        while($productModel->where('slug',$slug)->find() != null){
            $slug = increment_string($slug, '_');
        }
        $this->attributes['slug'] = $slug;
    }

    public function getCategorias()
    {
        $cpModel = model('CategoriesProductsModel');
        return $cpModel->where('product_id', $this->id)->join('categories', 'categories.id = products_categories.category_id')->findAll() ?? [];
    }

    public function getLink()
    {
        if ($this->file) {
            return base_url('uploads/imgs/'. $this->file);
        }
        
    }
    public function getLinkTiendaProducto()
    {
        return base_url(route_to('tienda_producto', $this->id));
    }
    public function getShowLink()
    {
        //cambiar por slug despues
        return base_url(route_to('products_ver', $this->id));
    }

    public function getEditLink()
    {
        return base_url(route_to('products_edit', $this->id));
    }

    public function getDeleteLink()
    {
        return base_url(route_to('products_delete', $this->id));
    }

    public function getComprarLink()
    {
        return base_url(route_to('tienda_comprar', $this->id));
    }
}
