<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class User extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];



    public function getUsername()
    {
        return $this->attributes['username'];
    }
    protected function setPassword(string $pwd)
    {
        $this->attributes['password'] = password_hash($pwd, PASSWORD_DEFAULT);
    }
    public function generateUsername()
    {
        $this->attributes['username'] = explode(" ", $this->name)[0] . explode(" ", $this->surname)[0];
    }

    public function getRole()
    {
        $model = model('GroupsModel');
        return $model->where('id', $this->group)->first();

    }

    public function getInfoUser()
    {
        $model = model('InfoUsersModel');
        return $model->where('id_user', $this->id)->first();
    }
  
}
