<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class VentasDetallesEntity extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at'];
    protected $casts   = [];

    public function getShowLink()
    {
        //cambiar por slug despues
        return base_url(route_to('products_ver', $this->producto_id));
    }
}