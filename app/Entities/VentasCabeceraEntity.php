<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class VentasCabeceraEntity extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at'];
    protected $casts   = [];


    
    public function getUser()
    {
        $model = model('InfoUsersModel');
        $m = $model->find($this->user_id);

        $name = $m->name ?? ' ';
        $surname = $m->surname ?? ' ';

        return $name. " ". $surname;
       
    }

    public function getLinkDetallesCompra()
    {
        return base_url(route_to('ventas_detalle', $this->id));
    }

}