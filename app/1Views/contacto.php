<?php echo $this->extend('base.php'); ?>

<?php echo $this->section('content'); ?>
<main class="container-xl py-5">
        <h2 class="text-center my-5">Contactanos</h2>
        <div class="row justify-content-center">
            <div class="col-md-10">

                <div class="row justify-content-center">
                    <div class="order-3 order-md-0 pt-4 pt-md-0 mt-md-0 mt-4 col-md-6">
                        <h3 class="bg-white text-center mt-md-0 mb-4">Direccion</h3>

                        <div class="mapa ">
                            <iframe 
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4366.496345784744!2d-58.84267514493467!3d-27.465147647955188!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94456ca532967881%3A0x57deeabb8995d079!2sBuenos%20Aires%20745%2C%20W3400BMH%20Corrientes!5e0!3m2!1ses!2sar!4v1713567521474!5m2!1ses!2sar" 
                                width="100%" height="400" 
                                style="border:0;" 
                                allowfullscreen="" 
                                loading="lazy" 
                                referrerpolicy="no-referrer-when-downgrade">
                            </iframe>
                        </div>
                        <div class="direccion">
                            <p class="pt-5"><span class="fw-bold">Direccion: </span> Buenos Aires 777 &, H3500 Resistencia, Chaco</p>
                        </div>
                        <div class="contacto">
                            <h3 class="bg-white text-center mb-4">Contacto</h3>
                            <p>Horario de Atencion:</p>
                            <p class="pt-1"><span class="fw-bold">Lunes a Viernes: </span> De 06.00 hs a 21.00 hs.</p>
                            <p class="pt-1"><span class="fw-bold">Sabados: </span> De 08.00 hs. a 13.00 hs.</p>
                            <p class="pt-1"><span class="fw-bold">Telefono: </span> 777-777-777</p>
                            <p class="pt-1"><span class="fw-bold">Email: </span> tiendademuebles@tienda.com</p>

                        </div>
                    </div>
                    <form class=" col-md-6 ps-5 needs-validation" novalidate>
                        <fieldset>
                            <legend class="bg-primary text-center text-white fs-5">Tus Datos</legend>

                            <div class="mb-3">
                                <label class="fs-6 form-label" for="nombre">Nombre:</label>
                                <input type="text" class="fs-6 form-control" id="nombre" placeholder="Tu Nombre" required>
                                <span class="invalid-feedback">Hubo un error...</span>
                            </div>
                            <div class="mb-3">
                                <label class="form-label fs-6" for="asunto">Asunto:</label>
                                <input type="text" class="fs-6 form-control" id="asunto" placeholder="Tu Asunto">
                                <span class="valid-feedback">Muy bien!!</span>
                            </div>
                            <div class="mb-3">
                                <label class="form-label" for="email">Email:</label>
                                <input type="email" class="form-control" id="email" placeholder="Tu Email">
                            </div>
                            <div class="mb-3">
                                <label class="form-label" for="tel">Teléfono:</label>
                                <input type="tel" class="form-control" id="tel" placeholder="Tu Teléfono">
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="tel">Mensaje:</label>
                                <textarea class="form-control" rows="10"></textarea>
                            </div>
                        </fieldset>

                        <fieldset>
                            <legend class="bg-primary text-center text-white fs-5">País</legend>

                            <div class="mb-3">
                                <label class="form-label" for="pais">País:</label>
                                <select class="form-control" id="pais">
                                    <option value="">--Seleccione--</option>
                                    <option value="MX">México</option>
                                    <option value="PR">Perú</option>
                                    <option value="CO">Colombia</option>
                                    <option value="AR">Argentina</option>
                                    <option value="ES">España</option>
                                    <option value="CL">Chile</option>
                                </select>
                            </div>
                        </fieldset>

                        <fieldset>
                            <legend class="bg-primary text-center text-white fs-5">Información Extra</legend>

                            <div class="mb-3">
                                <label class="form-label" for="cliente">Cliente:</label>
                                <input name="tipo" type="radio" class="form-check-input" id="cliente" >
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="proveedor">Proveedor:</label>
                                <input name="tipo" type="radio" class="form-check-input" id="proveedor" >
                            </div>

                        </fieldset>

                        <div class="d-grid d-md-flex">
                            <input class="btn btn-header" type="submit" value="Enviar Formulario">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
<?php echo $this->endSection(); ?>


<?php echo $this->section('js'); ?>
    <script></script>
<?php echo $this->endSection(); ?>