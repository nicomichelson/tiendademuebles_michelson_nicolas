<?php echo $this->extend('base.php'); ?>

<?php echo $this->section('content'); ?>
    <main class="container-xl py-5">
    
        <div class="row justify-content-center py-5 my-5">
            <section class="col-md-8 bg-white rounded-5 shadow">
                <div class="p-5 ">
                    <h2 class="text-center my-5">Sobre Nosotros</h2>

                    <div class="">
                        <img class="img-fluid" src="assets/img/nosotros.jpg" alt="imagen sobre nosotros">
                    </div>

                    <div class="py-5">
                        <p>
                            Somos una empresa especializada en la venta de muebles de pino de alta calidad. Nos esforzamos por ofrecer muebles duraderos y estéticamente atractivos que se adapten a cualquier estilo de decoración.
                            <br>
                            Nuestra empresa fue fundada hace 25 años por un carpintero apasionado por el trabajo con la madera. Empezamos con la fabricación de muebles de pino para clientes locales, pero pronto nos dimos cuenta de que había una gran demanda de muebles de pino de alta calidad. Con el tiempo, hemos expandido nuestra empresa para incluir una amplia gama de productos, desde mesas y sillas hasta armarios y estanterías.
                            <br>
                            En nuestra empresa, valoramos la calidad, la sostenibilidad y el servicio al cliente. Trabajamos con proveedores locales y utilizamos madera de pino sostenible para minimizar nuestro impacto ambiental. Además, nos esforzamos por brindar un servicio excepcional a nuestros clientes en todo momento.
                            <br>
                            Nuestro equipo está compuesto por un grupo de profesionales apasionados por el trabajo con la madera. Cada uno de nosotros tiene una experiencia única en la fabricación de muebles de pino.
                            <br>
                            Nuestra misión es ofrecer muebles de pino de alta calidad que se adapten a cualquier estilo de decoración y que duren muchos años. Nuestra visión es convertirnos en un referente en la venta de muebles de pino a nivel nacional, ofreciendo un servicio excepcional y productos de calidad.
                            <br>    
                            ¡Estamos siempre dispuestos a ayudarte! Puedes contactarnos a través de nuestro correo electrónico [dirección de correo], teléfono [número de teléfono] o visitando nuestra tienda física en [dirección de la tienda].
                        </p>
                    </div>
                </div>
            </section>    
        </div>
    </main> 
        

       
<?php echo $this->endSection(); ?>


<?php echo $this->section('js'); ?>
    <script></script>
<?php echo $this->endSection(); ?>