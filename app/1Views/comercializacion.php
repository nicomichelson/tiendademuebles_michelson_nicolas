<?php echo $this->extend('base.php'); ?>

<?php echo $this->section('content'); ?>

    <main class="container-xl">
        <div class="row justify-content-center py-5 my-5">
            <section class="col-md-8 bg-white rounded-5 shadow">
                <div class="p-5 ">
                    <h2 class="text-center fw-bold text-uppercase pb-4">Comercializacion</h3>
                    <p>
                    PLAZOS
                    <br>
                    La demora habitual en la producción y entrega de nuestros muebles es la siguiente:
                    </p>
                    <p>
                        • Productos en stock con entrega inmediata: 7 a 15 días. Motivos: control final y coordinación de envío.
                        <br>
                        • Productos a pedido: 30 a 60 días. Motivos: vamos por orden de pedido.
                    </p>

                    <p>Si no está aclarado en stock, se presume el mueble es a pedido.</p>

                    <p>
                        Por demoras en reposición de insumos o mano de obra PODEMOS EXTENDERNOS HASTA 30 DIAS MAS. En este caso se avisará de la demora adicional dentro de los plazos pactados. También puede haber demoras por feriados o vacaciones. No es lo habitual.
                        <br>
                        EN ESTOS PLAZOS NO ESTÁN INCLUIDOS LOS TIEMPOS DE ENVÍO. Una vez que el mueble es entregado a los transportes, la demora que puedan tener los muebles en llegar a destino depende exclusivamente de ellos.
                    </p>

                    <p>
                        <br>
                        ENVÍOS
                        <br>
                        Para envíos se debe abonar un adicional de costo de embalaje.
                        <br> 
                        Los envíos son a convenir con transportes. Su valor suele ser un 10% del valor declarado de los muebles.
                        <br>
                        Los horarios de retiro y entrega de muebles son los siguientes:
                        <br>
                        - Lunes a viernes 9.00 a 13.00 y 17.30 a 21.00 hs.
                        <br>
                        - Sábados 9.00 a 13.00 hs.
                        <br>
                        Los transportes cuentan con un seguro propio que se responsabiliza por los daños que puedan sufrir los muebles durante los traslados. ESTOS DAÑOS NO ESTAN INCLUIDOS EN NUESTRA GARANTÍA.
                    </p>
                    

                    <p>
                        GARANTÍA
                        <br>
                        Plazo:
                        <br>
                        Otorgamos garantia por 3 meses desde la fecha de entrega.
                    </p>
                    <br>

                    <p>
                        Funcionamiento:
                        <br>
                        Hecho el reclamo, se ingresa a una lista de espera de arreglos. Vamos por orden de reclamo.
                        <br>
                        Cuando llegue el turno del arreglo, nos comunicaremos para coordinar el envío del mueble o la ida a reparar en domicilio.
                        <br>
                        El comprador deberá ocuparse de enviarnos el mueble al taller para su reparación.
                        <br>
                        La reparación del mueble no tiene costo alguno durante el plazo de vigencia de la garantía.
                        <br>
                        La garantía no cubre el traslado de los muebles al taller para su reparación, ni el envío posterior al comprador para la devolcuión del mueble arreglado.
                        <br>
                        El plazo que nos lleve hacer la reparación del mueble será informado al comprador una vez evaluado el daño sufrido. Puede ir de 15 a 60 días. Si huebiera demora adicional por demora en reposición de insumos o mano de obra, se avisará dentro del plazo pactado.
                    </p>
                    <br>
                    <p>
                        • ¿Qué cubre nuestra garantía?
                        <br>
                        Cubre los daños que puedan sufrir nuestros muebles por su condición natural de estar fabricados en MADERA.
                        <br>
                        Esto significa que la madera puede “trabajar”: doblarse, abrirse o achicarse. 
                    </p>

                    <p>
                        • ¿Qué no cubre nuestra garantía?
                    <br>
                        No cubre los daños ocasionados por el uso de los muebles. Por ejemplo golpe, rayadura, quemadura.
                    <br>
                        LA GARANTÍA NO CUBRE LOS ENVÍOS. Cada transporte se responsabiliza por los daños que puedan sufrir los muebles durante los traslados. Ellos cuentan con un  seguro propio y los reclamos deben realizarse ante ellos.
                    </p>
                </div>
            </section>
        </div>

    </main>
    <section class=" bg-light  py-5">
        <div class="container-xl">

            <?= $this->include('includes/datos_pago') ?>
            
        </div>
    </section>

<?php echo $this->endSection(); ?>


<?php echo $this->section('js'); ?>
    <script></script>
<?php echo $this->endSection(); ?>