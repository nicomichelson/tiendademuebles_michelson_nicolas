<?php echo $this->extend('base.php'); ?>

<?php echo $this->section('content'); ?>

<div class="container-xl">
        <div class="row justify-content-center py-5 my-5">
            <section class="col-md-8 bg-white rounded-5 shadow">
                <div class="p-5 ">
                    <h2 class="text-center fw-bold text-uppercase pb-4">TÉRMINOS Y CONDICIONES DE USO</h3>

                    <p>
                        Esta página web es propiedad y está operado por <span class="text-primary fw-bold"> Tienda De Muebles </span>.
                        Estos Términos establecen los términos y condiciones bajo los cuales puedes usar nuestra
                        página web y nuestros servicios. Esta página web ofrece a los visitantes [descripción de lo
                        que se ofrece en la web]. Al acceder o usar la página web, aceptas haber leído, entendido y
                        aceptado estar sujeto a estos Términos:
                    </p> 
                    <br>
                    
                    <p>
                        <h5 class="fw-bold">INFORMACIÓN RELEVANTE</h1>
                        
                        En algunos casos, para adquirir un producto, será necesario el registro por parte del usuario,
                        con ingreso de datos personales fidedignos y definición de una contraseña. El usuario puede
                        elegir y cambiar la clave para su acceso de administración de la cuenta en cualquier
                        momento, en caso de que se haya registrado y que sea necesario para la uso de alguno de
                        nuestros servicios. <span class="text-primary fw-bold"> Tienda De Muebles </span> no asume la responsabilidad en caso de que
                        entregue dicha clave a terceros.
                        <br>
                        Todas las transacciones que se lleven a cabo por medio de este sitio web, están sujetas a un
                        proceso de confirmación y verificación, el cual podría incluir la validación de la forma de
                        pago, validación de la factura (en caso de existir) y el cumplimiento de las condiciones
                        requeridas por el medio de pago seleccionado. En algunos casos puede que se requiera una
                        verificación por medio de correo electrónico.
                        <br>
                        Los precios de los servicios ofrecidos en esta web son válidos solamente en las suscripciones
                        realizadas en este sitio web.
                        <br>
                        <br>
                        <h5 class="fw-bold">LICENCIA</h5>
                        [Nombre de la página web] a través de su sitio web concede una licencia para que los
                        usuarios utilicen los servicios que son vendidos en este sitio web de acuerdo a los Términos
                        y Condiciones que se describen en este documento.
                        <br>
                        <br>
                        <h5 class="fw-bold">USO NO AUTORIZADO</h5>
                        En caso de que aplique (para venta de software, templetes, u otro producto de diseño y
                        programación) usted no puede colocar uno de nuestros productos, modificado o sin
                        modificar, en un CD, sitio web o ningún otro medio y ofrecerlos para la redistribución o la
                        reventa de ningún tipo.
                        <br>
                        <br>
                        <h5 class="fw-bold">PROPIEDAD</h5>
                        Usted no puede declarar propiedad intelectual o exclusiva a ninguno de nuestros productos,
                        modificado o sin modificar. Todos los productos son propiedad de los proveedores del
                        contenido.  
                    </p>


                </div>
            </section>
        </div>

    </div>
    <section class=" bg-light  py-5">
        <div class="container-xl">

            <?= $this->include('includes/datos_pago') ?>
            
        </div>
    </section>

<?php echo $this->endSection(); ?>


<?php echo $this->section('js'); ?>
    <script></script>
<?php echo $this->endSection(); ?>