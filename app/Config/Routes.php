<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index', ['as' => 'home']);



$routes->group('auth', ['namespace' => 'App\Controllers\Auth'], function($routes){
    $routes->get('registro', 'Register::index', ['as' => 'register']);
    $routes->post('store', 'Register::store');
    $routes->get('login', 'Login::index', ['as' => 'login']);
    $routes->post('check', 'Login::signin', ['as' => 'signin']);
    $routes->get('logout', 'Login::signout', ['as' => 'signout']);


});

$routes->group('admin', ['namespace' => 'App\Controllers\Admin', 'filter' => 'auth:admin'], function($routes){
    $routes->get('/', 'DashboardController::index', ['as' => 'dashboard']);
    // $routes->get('productos', 'ProductosController::index', ['as' => 'productos']);
    $routes->get('usuarios', 'UsuariosController::index', ['as' => 'users']);

    //category
    $routes->get('categorias', 'CategoriesController::index', ['as' => 'categories']);
    $routes->get('categorias/crear', 'CategoriesController::create', ['as' => 'categories_create']);
    $routes->post('categorias/guardar', 'CategoriesController::store', ['as' => 'categories_store']);
    $routes->get('categorias/editar/(:any)', 'CategoriesController::edit/$1', ['as' => 'categories_edit']);
    $routes->post('categorias/update', 'CategoriesController::update', ['as' => 'categories_update']);
    $routes->get('categorias/delete/(:any)', 'CategoriesController::delete/$1', ['as' => 'categories_delete']);


    //productos
    $routes->get('productos', 'ProductosController::index', ['as' => 'productos']);
    $routes->get('productos/crete', 'ProductosController::create', ['as' => 'products_create']);
    $routes->post('productos/guardar', 'ProductosController::store', ['as' => 'products_store']);
    $routes->get('productos/editar/(:any)', 'ProductosController::edit/$1', ['as' => 'products_edit']);
    $routes->post('productos/update', 'ProductosController::update', ['as' => 'products_update']);
    $routes->get('productos/show/(:any)', 'ProductosController::ver/$1', ['as' => 'products_ver']);
    $routes->get('productos/delete/(:any)', 'ProductosController::delete/$1', ['as' => 'products_delete']);

    $routes->get('ventas', 'VentasController::index', ['as' => 'ventas_cabecera']);
    $routes->get('ventas/detalle/(:any)', 'VentasController::detalle/$1', ['as' => 'ventas_detalle']);

});

$routes->group('migration', ['namespace' => 'App\Controllers\Migrations'], function($routes){
    
    $routes->get('migration', 'MigrationController::createMigration', ['as' => 'migration']);
    $routes->get('seeder', 'MigrationController::createSeeder', ['as' => 'seeder']);
    


});

$routes->group('tienda', ['namespace' => 'App\Controllers'], function($routes){

    $routes->get('carrito', 'TiendaController::listarCarrito', ['as' => 'tienda_carrito']);
    $routes->get('/', 'TiendaController::index');
    $routes->get('producto/(:any)', 'TiendaController::producto/$1', ['as' => 'tienda_producto'] );
    $routes->get('comprar/(:any)', 'TiendaController::comprar/$1', ['as' => 'tienda_comprar'] );
    
});

$routes->group('tienda', ['namespace' => 'App\Controllers'], function($routes){

    $routes->post('venta', 'TiendaController::venta', ['as' => 'tienda_venta'] );
    $routes->get('compraUsuario', 'TiendaController::compraUsuario', ['as' => 'detalle_compra_usuario']);
});



$routes->get('nosotros', 'Home::nosotros');
$routes->get('contacto', 'Home::contacto');
$routes->get('politicas_empresa', 'Home::politicasPrivacidad');
$routes->get('comercializacion', 'Home::comercializacion');
$routes->get('terminos', 'Home::terminos');