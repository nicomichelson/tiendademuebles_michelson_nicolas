<?php
namespace Config;

use CodeIgniter\Config\BaseConfig;

class BlogConfig extends BaseConfig
{
    public $defaultGroupUsers = 'user';
    public $regPerPage = 12;

}