<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class GroupSeeder extends Seeder
{
    public function run()
    {
        $groups = [
            [
                'name' => 'admin'
            ],
            [
                'name' => 'user'
            ]
        ];

        $builder = $this->db->table('groups');
        $builder->insertBatch($groups);

    }
}
