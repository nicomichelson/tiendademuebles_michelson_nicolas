<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Faker\Factory;

class InitSeeders extends Seeder
{
    
    public function run()
    {
        $this->call('ProductsSeeder');
        $this->call('CategoriesSeeder');
        $this->call('CategoryProductSeeder');
        $this->call('GroupSeeder');
        $this->call('UserSeeder');
    }
}
