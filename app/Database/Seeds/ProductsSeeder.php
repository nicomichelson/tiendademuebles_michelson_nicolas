<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Faker\Factory;


class ProductsSeeder extends Seeder
{
    public function run()
    {
        // $faker = Factory::create();
        
        $products = [];
        // $created_at = $faker->dateTimeAD();

        for ($i=0; $i <= 14  ; $i++) { 
            
            $products[] = [
                'name' => 'Producto Mueble '.$i,
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum ',
                'stock' => rand(4,20),
                'stock_min' => 2,
                'precio' => mt_rand (100*10, 10000*10) / 10,
                'precio_vta' => mt_rand (100*10, 10000*10) / 10
                // 'created_at' => $created_at
            ];

        }

        $builder = $this->db->table('products');
        $builder->insertBatch($products);
    }
}
