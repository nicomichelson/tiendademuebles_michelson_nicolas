<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CategoryProductSeeder extends Seeder
{
    public function run()
    {
        $arr = [];
        for ($i=0; $i <= 20 ; $i++) { 
            $arr[] = [
                'category_id' => rand(1,4),
                'product_id' => rand(1,15),
            ];
        }

        $builder = $this->db->table('products_categories');
        $builder->insertBatch($arr);
    }
}
