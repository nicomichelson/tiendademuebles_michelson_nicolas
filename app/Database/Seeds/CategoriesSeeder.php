<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    

    public function run()
    {
        $categories = [];
        foreach ($this->categoriesFake() as $key => $value) {
            $categories[] = [
                'name' => $value
            ];
        }


        $builder = $this->db->table('categories');
        $builder->insertBatch($categories);
    }


    private function categoriesFake(){

        return ["Oficina", "Living", "Hogar", "Cocina"];
    }


}



