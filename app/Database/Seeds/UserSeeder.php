<?php

namespace App\Database\Seeds;

use App\Entities\User;
use App\Entities\UserInfo;
use CodeIgniter\Database\Seeder;
use Faker\Factory;


class UserSeeder extends Seeder
{
    public function run()
    {
        $c =config('BlogConfig');
        $user1 = [
            'name' => 'user',
            'surname' => 'user',
            'email' => 'user@user.com',
            'password' => 'user',
            'c-password' => 'user'
        ];
        $admin1 = [
            'name' => 'admin',
            'surname' => 'admin',
            'email' => 'admin@admin.com',
            'password' => 'admin',
            'c-password' => 'admin'
        ];

        

        $user = new User($user1);

        $user->generateUsername();
        $model = model('UserModel');
       
        $model->withGroup('user');

        $userInfo = new UserInfo($user1);
        $model->addUserInfo($userInfo);


        $model->save($user);

        $admin = new User($admin1);

        $admin->generateUsername();
        $model = model('UserModel');
       
        $model->withGroup('admin');

        $adminInfo = new UserInfo($admin1);
        $model->addUserInfo($adminInfo);


        $model->save($admin);

        
    }

    private function getUserData(): array
    {
        return [

            
            ['admin@admin.com','admin', 'admin', 1, 'admin1','admin1'],
            ['user@user.com','user', 'user', 2, 'user2','user2'],

        ];
    }
}
