<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Producto extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'description' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'file' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
            ],
            'slug' => [
                'type'           => 'VARCHAR',
                'constraint'     => '100',
                'null'       => true,
                
            ],
            'stock' => [
                'type'           => 'INT',
                'constraint'     => '11',
                'null'       => TRUE,
                                
            ],
            'stock_min' => [
                'type'           => 'INT',
                'constraint'     => '11',
                'null'       => TRUE,
                                
            ],
            'precio' => [
                'type'           => 'DECIMAL',
                'constraint'     => '10,3',
                'null'       => FALSE,
                'default'   => 0.00
                
            ],
            'precio_vta' => [
                'type'           => 'DECIMAL',
                'constraint'     => '10,3',
                'null'       => FALSE,
                'default'   => 0.00
                
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => false
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => false
            ],
            'deleted_at' => [
                'type' => 'DATETIME',
                'null' => false
            ]
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('products');
    }

    public function down()
    {
        $this->forge->dropTable('products');
    }
}
