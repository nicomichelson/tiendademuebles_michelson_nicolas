<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AgregarStatusProducto extends Migration
{
    public function up()
    {
        $data = [
            'status' => [
                'type' => 'INT',
                'constraint'     => '11',
                'null'       => false,
                'after' => 'precio_vta'
            ]
            ];
        $this->db->disableForeignKeyChecks();
        $this->forge->addColumn('products', $data);
        $this->db->enableForeignKeyChecks();
    }

    public function down()
    {
        //
    }
}
