<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VentasDetalle extends Migration
{
    public function up()
    {
        $this->db->disableForeignKeyChecks();
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 12,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'venta_id' => [
                'type'           => 'INT',
                'constraint'     => 12,
                'unsigned'       => true,
                'null' => true,
            ],
            'producto_id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'null' => true,
            ],
            'cantidad' => [
                'type'           => 'INT',
                'constraint'     => '11',
                'null'       => TRUE,
                                
            ],
            'precio' => [
                'type'           => 'DECIMAL',
                'constraint'     => '10,3',
                'null'       => FALSE,
                'default'   => 0.00
                
            ],
         
            'created_at' => [
                'type' => 'DATETIME',
                'null' => false
            ],
           
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('venta_id','ventas_cabecera', 'id', 'CASCADE','SET NULL');
        $this->forge->addForeignKey('producto_id','products', 'id', 'CASCADE','SET NULL');
        $this->forge->createTable('ventas_detalle');
        $this->db->enableForeignKeyChecks();
    }

    public function down()
    {
        $this->forge->dropTable('ventas_detalle');
    }
}
