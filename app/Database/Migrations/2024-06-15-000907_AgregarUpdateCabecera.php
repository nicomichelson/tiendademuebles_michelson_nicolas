<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AgregarUpdateCabecera extends Migration
{
    public function up()
    {
        $data = [
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => false
            ],
            ];
        $this->db->disableForeignKeyChecks();
        $this->forge->addColumn('ventas_cabecera', $data);
        $this->db->enableForeignKeyChecks();
    }

    public function down()
    {
        //
    }
}
