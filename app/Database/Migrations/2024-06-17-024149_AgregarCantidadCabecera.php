<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AgregarCantidadCabecera extends Migration
{
    public function up()
    {
        $data = [
            'cantidad' => [
                'type' => 'int',
                'constraint'     => '11',
                'null' => true
            ],
            ];
        $this->db->disableForeignKeyChecks();
        $this->forge->addColumn('ventas_cabecera', $data);
        $this->db->enableForeignKeyChecks();
    }

    public function down()
    {
        //
    }
}
