<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class VentasCabecera extends Migration
{
    public function up()
    {
        $this->db->disableForeignKeyChecks();
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 12,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'user_id' => [
                'type'           => 'INT',
                'constraint'     => 12,
                'unsigned'       => true,
                'null' => true,
            ],
         
            'total_venta' => [
                'type'           => 'DECIMAL',
                'constraint'     => '10,3',
                'null'       => FALSE,
                'default'   => 0.00
                
            ],
         
            'created_at' => [
                'type' => 'DATETIME',
                'null' => false
            ],
           
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('user_id','users', 'id', 'CASCADE','SET NULL');
        $this->forge->createTable('ventas_cabecera');
        $this->db->enableForeignKeyChecks();
    }

    public function down()
    {
        $this->forge->dropTable('ventas_cabecera');
    }
}
