<?php echo $this->extend('base.php'); ?>

<?php echo $this->section('content'); ?>
    
    <section class="container-xl my-0 my-md-5">
        <?= $this->include('includes/carrousel') ?>
    </section>


    <?= $this->include('includes/productos_principal') ?>

    <section class="  py-md-5">
        <div class="container-xl">

        </div>
    </section>
    
    <!-- <main class="container-xl py-5">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h1 class="text-center ">Nuestros Productos</h1>

                <div class="row g-0 pt-4 mb-4">
                    <div class="col-md-8 producto">
                        <img class="img-fluid" src="assets/img/producto1.jpg" alt="imagen producto">
                    </div>
                    <div class="col-md-4 bg-primary text-center p-1 text-white d-flex flex-column justify-content-center">
                        <h2>Producto 1</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam deleniti sed recusandae vitae architecto molestiae, eos corrupti</p>
                        <p class="fs-1 fw-bold">$12,000.00</p>
                        <a class="btn btn-success  fw-bold text-white text-uppercase py-3" href="#">Agregar al carrito</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 mb-4">
                        <div class="card">
                            
                            <img class="card-img-top" src="assets/img/producto2.jpg" alt="imagen producto">

                            <div class="card-body text-center bg-primary text-white p-5">
                                <h3>Producto 2</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam deleniti sed recusandae vitae architecto molestiae, eos corrupti</p>
                                <p class="fs-1 fw-bold">$12,000.00</p>
                                <a class="btn btn-success text-white fw-bold text-uppercase py-3 w-100" href="#">Agregar al carrito</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-4">
                        <div class="card">
                            <img class="card-img-top" src="assets/img/producto3.jpg" alt="imagen producto">

                            <div class="card-body text-center bg-primary text-white p-5">
                                <h3>Producto 3</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam deleniti sed recusandae vitae architecto molestiae, eos corrupti</p>
                                <p class="fs-1 fw-bold">$12,000.00</p>
                                <a class="btn btn-success text-white fw-bold text-uppercase py-3 w-100" href="#">Agregar al carrito</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 mb-4">
                        <div class="card">
                            <img class="card-img-top" src="assets/img/producto4.jpg" alt="imagen producto">

                            <div class="card-body text-center bg-primary text-white p-5">
                                <h3>Producto 4</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam deleniti sed recusandae vitae architecto molestiae, eos corrupti</p>
                                <p class="fs-1 fw-bold">$12,000.00</p>
                                <a class="btn btn-success text-white fw-bold text-white text-uppercase py-3 w-100" href="#">Agregar al carrito</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mb-4">
                        <div class="card">
                            <img class="card-img-top" src="assets/img/producto5.jpg" alt="imagen producto">

                            <div class="card-body text-center bg-primary text-white p-5">
                                <h3>Producto 5</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam deleniti sed recusandae vitae architecto molestiae, eos corrupti</p>
                                <p class="fs-1 fw-bold">$12,000.00</p>
                                <a class="btn btn-success fw-bold text-white text-uppercase py-3 w-100" href="#">Agregar al carrito</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mb-4">
                        <div class="card">
                            <img class="card-img-top" src="assets/img/producto6.jpg" alt="imagen producto">

                            <div class="card-body text-center bg-primary text-white p-5">
                                <h3>Producto 6</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam deleniti sed recusandae vitae architecto molestiae, eos corrupti</p>
                                <p class="fs-1 fw-bold">$12,000.00</p>
                                <a class="btn btn-success  text-white fw-bold text-uppercase py-3 w-100" href="#">Agregar al carrito</a>
                            </div>
                        </div>
                    </div>
                </div>

            
            </div>
        </div>
        

        

       
        
    </main> -->
    <section class="container-xl categorias  py-5">
        <p class="fs-4 fw-bold">Categorías Principales</p>
        <ul class="list-unstyled grid">
            
            <li class="g-col-6 g-col-md-4 g-col-lg-3">
                <a href="#" class="text-decoration-none">
                    <div class="bg-light">
                        <img class="img-fluid" src="assets/img/categoria1.jpg" alt="Imagen Icono">
                    </div>
                    <p class="fw-bold mt-2">Oficina</p>
                </a>
            </li>

            <li class="g-col-6 g-col-md-4 g-col-lg-3">
                <a href="#" class="text-decoration-none">
                    <div class="bg-light">
                        <img class="img-fluid" src="assets/img/categoria2.jpg" alt="Imagen Icono">
                    </div>
                    <p class="fw-bold mt-2">Hogar</p>
                </a>
            </li>

            <li class="g-col-6 g-col-md-4 g-col-lg-3">
                <a href="#" class="text-decoration-none">
                    <div class="bg-light">
                        <img class="img-fluid" src="assets/img/categoria1.jpg" alt="Imagen Icono">
                    </div>
                    <p class="fw-bold mt-2">Cocina</p>
                </a>
            </li>
            <li class="g-col-6 g-col-md-4 g-col-lg-3">
                <a href="#" class="text-decoration-none">
                    <div class="bg-light">
                        <img class="img-fluid" src="assets/img/categoria3.jpg" alt="Imagen Icono">
                    </div>
                    <p class="fw-bold mt-2">Living</p>
                </a>
            </li>

        </ul>
    </section>
   
    <section class=" bg-light  py-5">
        <div class="container-xl">

            <?= $this->include('includes/datos_pago') ?>
            
        </div>
    </section>
   
<?php echo $this->endSection(); ?>


<?php echo $this->section('js'); ?>
    <script src="assets/js/mostrarProductos.min.js"></script>
<?php echo $this->endSection(); ?>

