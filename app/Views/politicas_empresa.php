<?php echo $this->extend('base.php'); ?>

<?php echo $this->section('content'); ?>
<main class="container-xl py-5">
        <h1 class="text-center my-5">Política de privacidad y seguridad de datos</h1>
        <h3 class="text-center my-5">Toda la información sobre la infraestructura de Privacidad y Seguridad de Tienda de Muebles</h2>
        
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="grid gap-0 gap-4 infocards">
                    <div class="p-1 g-col-12 g-col-md-4 bg-white rounded-5 infocard shadow p-3 mb-5">
                        <div class="infocard__body seguridad">
                            <h3 class="text-center">Seguridad</h3>
                            <p class="text-center">Tienda de Muebles está certificado y cumple con los requisitos establecidos por la norma internacional ISO/IEC 27001.</p>
                            
                        </div>
                        <div class="d-flex justify-content-center">
                            <a href="#" class="text-center fw-bold">Ver mas</a>
                        </div>
                    </div>
                    <div class="p-1 g-col-12 bg-white g-col-md-4 rounded-5 infocard shadow p-3 mb-5 ">
                        <div class="infocard__body privacidad">
                            <h3 class="text-center">Privacidad</h3>
                            <p class="text-center">Tienda de Muebles está certificado y cumple con los requisitos establecidos por la norma internacional ISO/IEC 27001.</p>
                        </div>
                        <div class="infocard__footer">
                            <a href="#" class="text-center fw-bold">Ver mas</a>
                        </div>
                    </div>
                    <div class="p-1 g-col-12 bg-white g-col-md-4 rounded-5 infocard shadow p-3 mb-5">
                        <div class="infocard__body terminos">
                            <h3 class="text-center">Terminos</h3>
                            <p class="text-center">Estas son las reglas que debes seguir para realizar tus compras en Tienda De muebles. Al utilizar los servicios de Tienda De Muebles, aceptas nuestros Términos y Condiciones.</p>
                        </div> 
                        <div class="infocard__footer">
                            <a href="/terminos" class="text-center fw-bold">Ver mas</a>
                        </div>
                           
                    </div>
                </div>
                              
            </div>
        </div>
    </main>
<?php echo $this->endSection(); ?>


<?php echo $this->section('js'); ?>
    <script></script>
<?php echo $this->endSection(); ?>