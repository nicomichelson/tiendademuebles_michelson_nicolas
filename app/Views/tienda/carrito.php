<?php echo $this->extend('base.php'); ?>

<?php echo $this->section('content'); ?>

<section  class="container-xl carritos-producto">
        <div class="alertas1">

        </div>
        <div class="row justify-content-center">
            <div id="lista-productos" class="col-md-8 my-5">
                <div id="producto">
                    <table id="lista-producto" class="table">
                        <thead>
                            <tr>
                                <th>Imagenss</th>
                                <th>Nombre</th>
                                <th>Precio</th>
                                <th>Cantidad</th>
                                <th>Sub total</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot></tfoot>
                    </table>
                            
                </div>
            </div>
            <div class="detalle-producto__comprar col-md-4 p-5 my-5 border border-dark-subtle rounded-3 ">
               
                <div id="total-producto" class="mb-5">
                    
                </div>
                
                <div class="botones-compra d-flex flex-column gap-3">
                    <a class="btn btn-dark comprar-ahora " href="#">Comprar Ahora</a>
                    <a href="#" class="  btn btn-outline-dark">Vaciar el Carrito</a> 
                </div>
                            
            </div>
        </div>
    

</section>

<?php echo $this->endSection(); ?>

<?php echo $this->section('js'); ?>
    
    <script src="<?= base_url('assets/js/mostrarCarrito.min.js')?>"></script>
    <script src="<?= base_url('assets/js/listarCarrito.min.js')?>"></script>

    <script>
        const btnComprar = document.querySelector('.comprar-ahora');
        btnComprar.addEventListener('click', comprarAhora);
        const alertas = document.querySelector('.alertas1');
        function comprarAhora()
        {
            // console.log('comprnado');
                
                const path= "<?= base_url(route_to('tienda_venta'))?>";
                const prod = localStorage.getItem('carrito');

                console.log(prod);
                //obtener datos localstorage
            fetch(path,{
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        "X-Requested-With": "XMLHttpRequest"
                    },
                    body: JSON.stringify({ carrito: prod }) 
                    
            })
                .then(respuesta => respuesta.json())
                .then(datos => mostrarAlerta(datos))
                .catch(err => console.log(err));
           
        }

        function mostrarAlerta(datos)
        {
            const alertaMensaje = document.createElement('DIV');
            

            if(!datos['logeado']){

                alertaMensaje.classList.add('alert', 'alert-warning');
                alertaMensaje.textContent= datos['message'];
            }

            if(datos['logeado']){

                alertaMensaje.classList.add('alert', 'alert-success');
                alertaMensaje.textContent= datos['message'];
            }

            alertas.appendChild(alertaMensaje);
        }

    </script>

<?php echo $this->endSection(); ?>