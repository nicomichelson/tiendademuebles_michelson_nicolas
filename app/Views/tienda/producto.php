<?php echo $this->extend('base.php'); ?>

<?php echo $this->section('content'); ?>

<section class="container-xl detalle-producto">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="row">
                <div class="col">
                    <img 
                        class="img-fluid py-5"
                        <?php if($producto->getLink() != null): ?>
                           
                        src="<?= $producto->getLink()?>"
                        <?php else: ?>
                           
                        src="assets/img/producto<?= rand(1,4)?>.jpg"
                        <?php endif; ?>
                        alt="imagen producto"
                    />
                </div>
            </div>
        </div>
        <div class="detalle-producto__descripcion col-md-4">
            <div class="py-5">
                <h5 class="mb-5"><?= $producto->name?></h5>
                <p class="m-0 fs-4 fw-bold">$<?= $producto->precio_vta?></p>
                <p class="m-0 fs-5">mismo precio en cuotas</p>
                <div class="mt-3">
                    <?= $producto->description?>
                </div>
                
            </div>
            
        </div>
        <div class="detalle-producto__comprar col-md-4 p-5 my-5 border border-dark-subtle rounded-3 ">
            <div class="stock mb-5">
                <p class="fw-bold">Stock disponible</p>
                <p>Cantidad: <span class="fs-6 fw-bold">1 Unidad </span> <span class="fw-lighter"> (<?= $producto->stock ?> disponible) </span></p>
            </div>
            <div class="botones-compra d-flex flex-column gap-3">
                <a class="btn btn-dark " href="<?=$producto->getComprarLink()?>">Comprar Ahora</a>
                <a href="#" class="btn btn-outline-dark">Agregar al Carrito</a> 
            </div>
                          
        </div>
    </div>

</section>

<?php echo $this->endSection(); ?>