<?php echo $this->extend('base.php'); ?>


<?php echo $this->section('content'); ?>
    <section class="container-xl">
        <div class="row justify-content-center py-5 my-5">
            <?php if(session('msg')): ?>
                <div class="alert alert-<?= session('msg.type') ?>" role="alert">
                    <?=session('msg.body')?>
                </div>
            <?php endif; ?>
            <div class="col-md-8 bg-white rounded-5 shadow">
                
                <h2>Login</h2>
                <div class="p-5 mx-5">
                    <form action="<?php echo base_url(route_to('signin')) ?>" method="POST">

                    <div class="form-group">
                            <label for="email">Email address:</label>
                            <input name="email" value="<?php old('email')?>" type="email" class="form-control" id="email">
                            <p class="text-danger"><?=session('errors.email')?></p>
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input name="pwd" type="password" class="form-control" id="pwd">
                            <p class="text-danger"><?=session('errors.pwd')?></p>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox"> Remember me</label>
                        </div>
                        
                        <input class="btn btn-primary" type="submit" value="Login">
                    </form>
                </div>
        
            </div>
        </div>
    </section>
<?php echo $this->endsection(); ?>

