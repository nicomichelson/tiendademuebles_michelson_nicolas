<?php echo $this->extend('base.php'); ?>


<?php echo $this->section('content'); ?>
    <section class="container-xl">
        <div class="row justify-content-center py-5 my-5">
            <div class="col-md-8 bg-white rounded-5 shadow">
                
                <form class="p-5" action="<?php echo base_url('auth/store')?>" method="POST">
                    <fieldset class="">

                        <div class="mb-3">
                            <label class="fs-6 form-label" for="name">Nombre:</label>
                            <input name="name" value="<?=old('name') ?>" type="text" class="fs-6 form-control" id="nombre" placeholder="Tu Nombre" required>
                            <span class="text-danger"><?=session('errors.name')?></span>
                        </div>
                        <div class="mb-3">
                            <label class="form-label fs-6" for="surname">Apellido:</label>
                            <input name="surname" value="<?=old('surname') ?>" type="text" class="fs-6 form-control" id="asunto" placeholder="Apellido">
                            <span class="text-danger"><?=session('errors.name')?></span>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="email">Email:</label>
                            <input name="email" value="<?=old('email') ?>" type="email" class="form-control" id="email" placeholder="Tu Email">
                            <span class="text-danger"><?=session('errors.email')?></span>
                        </div>
                        
                        <div class="mb-3">
                            <label class="form-label" for="pais">País:</label>
                            <select class="form-control" name="id_country" id="pais">
                                <option value="">--Seleccione--</option>
                                <option value="MX">México</option>
                                <option value="PR">Perú</option>
                                <option value="CO">Colombia</option>
                                <option value="AR">Argentina</option>
                                <option value="ES">España</option>
                                <option value="CL">Chile</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="password">Password:</label>
                            <input name="password" value="" type="password" class="form-control" id="tel" placeholder="Tu Password">
                            <span class="text-danger"><?=session('errors.password')?></span>
                        </div>

                        <div class="mb-3">
                            <label class="form-label" for="c-password">Confirmacion de password:</label>
                            <input name="c-password" value="" type="password" class="form-control" id="tel" placeholder="Tu confirmacion">
                            <span class="text-danger"><?=session('errors.password')?></span>
                        </div>

                    </fieldset>
                    <div class="d-grid d-md-flex">
                        <input class="btn btn-header" type="submit" value="Enviar Formulario">
                    </div>
                </form>
        
            </div>
        </div>
    </section>
<?php echo $this->endsection(); ?>

