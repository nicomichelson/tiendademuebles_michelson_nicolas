<?php echo $this->extend('base.php'); ?>


<?php echo $this->section('content'); ?>
    <section class="container-xl">
        <div class="row justify-content-center py-5 my-5">
            <div class="col-md-8 bg-white rounded-5 shadow">
                
                <h2>register list</h2>


                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">Handle</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td colspan="2">Larry the Bird</td>
                        <td>@twitter</td>
                        </tr>
                    </tbody>
                </table>
        
            </div>
        </div>
    </section>
<?php echo $this->endsection(); ?>

