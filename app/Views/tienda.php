<?php echo $this->extend('base.php'); ?>

<?php echo $this->section('content'); ?>

    <section class="  py-md-5">
        <div class="container-xl">

            <ul id="lista-cursos" class="list-unstyled mt-5 listado-productos">
                <?php foreach ($products as $v): ?>
                    
                    <li class="producto">
                        <a href="<?= $v->getLinkTiendaProducto() ?>" class="text-decoration-none">
                            <div class="card">
                                <img 
                                    class="card-img-top"
                                    
                                    <?php if($v->getLink() != null): ?>
                                        
                                        src="<?= $v->getLink()?>"
                                    <?php else: ?>
                                        src="assets/img/producto<?= rand(1,4)?>.jpg"
                                    <?php endif; ?>
                                        alt="imagen producto"
                                />

                                <div class="card-body">
                                    <h4 class="titulo"> <?= $v->name ?></h4>
                                    <p class="descripcion-producto m-0"><?= character_limiter($v->description,30) ?></p>
                                    <?php if(!empty($v->getCategorias())): ?>
                                        <?php foreach($v->getCategorias() as $c):?>
                                            <a href="#"> <?= $c->name ?> </a>   
                                        <?php endforeach;?>    
                                    <?php endif;?>
                                    <p 
                                        class="precio fw-bold"> 
                                        <?php if($v->precio_vta):?> 
                                           <span> <?= $v->precio_vta?></span>
                                        <?php else: ?>
                                            10 USD$
                                        <?php endif; ?> 
                                    </p>
                                    <a href="#"  class="btnCompras agregar-carrito" data-id="<?= $v->id ?>">Agregar al Carrito</a>
                                           
                                </div>
                            </div>
                        </a>
                    </li>
                <?php endforeach ?>
            </ul>
            <div class="d-flex justify-content-center">
                <?= $pager->links() ?>
            </div>
        </div>

        
    </section>

<?php echo $this->endSection(); ?>