<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href='<?php echo base_url("assets/img/logo-cortado.jpg")?>' type="img/jpg"/>
    <link rel="stylesheet" href='<?php echo base_url("assets/css/app.css")?>' />    
    <title>Base</title>
</head>
<body>
    <header class="header  bg-white py-3 shadow-sm">
        <?= $this->include('includes/_menu') ?>
    </header>
    <?= $this->include('includes/_cart') ?>
    <?php echo $this->renderSection('content'); ?>
    <?= $this->include('includes/footer') ?>
    <?php echo $this->renderSection('js'); ?>
    <script src="<?= base_url('assets/js/boostrap/js/bootstrap.bundle.min.js')?>"></script>
    <script src="<?= base_url("assets/js/carrito.min.js")?>"></script>
</body>
</html>