<div class="bg-light py-3">
    <div class="container-xl">
        <div class="row justify-content-center">
            <div class="col-8">
                <form action="#">
                    <input class="border-0 rounded-5" type="text" value="Buscar">
                </form>        
            </div>
            <div class="col-md-4">
                <ul>
                    <li class="submenu">
                        <div class="imagen-carrito">
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-cart" viewBox="0 0 16 16">
                                <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5M3.102 4l1.313 7h8.17l1.313-7zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4m7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4m-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2m7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2"/>
                            </svg>
                        </div>
                        <div id="carrito">
                            <table id="lista-carrito" class="table">
                                <thead>
                                    <tr>
                                        <th>Imagen</th>
                                        <th>Nombre</th>
                                        <th>Precio</th>
                                        <th>Cantidad</th>
                                        <th>Sub total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                
                            </table>
                            
                            <a href="#" id="vaciar-carrito" class="btn btn-outline-dark d-block">Vaciar Carrito</a>
                            <a href="<?= base_url(route_to('tienda_carrito'))?>" id="comprar-carrito" class="btn btn-dark d-block">Comprar</a>
                            
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        
    </div>
</div>