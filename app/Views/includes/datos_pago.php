<div class="row justify-content-center">
                
    <div class="col-md-10">

        <div class="grid gap-4">
            <div class="p-1 g-col-12 g-col-md-4  mb-5">
                <div class="infocard__body pagar">
                    <p class="fs-16px text-center fw-bold">Elegí cómo pagar</p>
                    <p class="fs-15px text-center">Podés pagar con tarjeta, débito, efectivo o hasta 12 cuotas</p>
                    <div class="d-flex justify-content-between">
                        <img src="assets/img/banelco.png" class="img-tarjeta" alt="img-tarjeta">
                        <img src="assets/img/mercadopago.png" class="img-tarjeta" alt="img-tarjeta">
                        <img src="assets/img/pagofacil.png" class="img-tarjeta" alt="img-tarjeta">
                        <img src="assets/img/naranja.png" class="img-tarjeta" alt="img-tarjeta">
                        <img src="assets/img/mastercard.png" class="img-tarjeta" alt="img-tarjeta">
                    </div>
                </div>
                <!-- <div class="d-flex justify-content-center">
                    <a href="#" class="fs-15px text-decoration-none text-center fw-bold">Como pagar tus compras</a>
                </div> -->
            </div>
            <div class="p-1 g-col-12 g-col-md-4  mb-5">
                <div class="infocard__body envio">
                    <p class="fs-16px text-center fw-bold">Comprá desde cualquier lugar</p>
                    <p class="fs-15px text-center">Sumá los productos que quieras al carrito. Te los llevamos hasta dónde estés.</p>
                    
                </div>
                <!-- <div class="d-flex justify-content-center">
                    <a href="#" class="fs-15px text-decoration-none text-center fw-bold">Conoce mas sobre este beneficio</a>
                </div> -->
            </div>
            <div class="p-1 g-col-12 g-col-md-4  mb-5">
                <div class="infocard__body privacidad1">
                    <p class="fs-16px text-center fw-bold">Recibí tus productos en menos de 48 hs</p>
                    <p class="fs-15px text-center">Tus paquetes están seguros. Tenés el respaldo de los envíos con Mercado Libre.</p>
                    
                </div>
                <!-- <div class="d-flex justify-content-center">
                    <a href="#" class="text-decoration-none fs-15px text-center fw-bold">Como te protegemos</a>
                </div> -->
            </div>
        </div>

    </div>
</div>