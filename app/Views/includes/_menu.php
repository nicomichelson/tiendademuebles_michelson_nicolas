<div class="container-xl">
    <nav class="navbar navbar-expand-lg ">

        <div class="container-fluid ">
            
            <a href="/" class="navbar-brand text-success fw-semibold fs-4">
                <div class="img-log">
                    <img src='<?php echo base_url("assets/img/logo-cortado.jpg") ?>' alt="Logotipo">
                </div>
            </a>
            <h3 class="offcanvas-title text-primary">Tienda de Muebles</h3> 
            <button 
                class="navbar-toggler"
                type="button" 
                data-bs-toggle="offcanvas"
                data-bs-target="#menuLateral"
            >
                <span class="navbar-toggler-icon"></span>
            </button>
            <section class="offcanvas offcanvas-start bg-light" id="menuLateral" tabindex="-1">
                <div class="offcanvas-header" data-bs-theme="dark">
                    <h5 class="offcanvas-title text-primary">Tienda de Muebles</h5> 
                    <a href="/" class="offcanvas-title text-success fw-semibold fs-4">
                        <div class="img-log">
                            <!-- <img src="#" alt="Logotipo "> -->
                        </div>
                    </a>
                    <button 
                        class="btn-close " 
                        type="button"
                        arial-label="Close"
                        data-bs-dismiss="offcanvas"
                    >
                    </button>
                </div>
                <div class="px-0 offcanvas-body d-flex flex-column flex-md-row align-item-md-center justify-content-between">
                    <ul class="navbar-nav flex-grow-1 justify-content-evenly">
                        <li class="p-3 px-md-0 nav-item py-md-1"><a href="<?php echo base_url(route_to('nosotros')) ?>" class="text-secondary nav-link text-uppercase fw-semibold">Quienes Somos</a></li>
                        <li class="p-3 px-md-0 nav-item py-md-1"><a href="<?php echo base_url(route_to('tienda')) ?>" class="text-secondary nav-link text-uppercase fw-semibold">Tienda</a></li>
                        <li class="p-3 px-md-0 nav-item py-md-1"><a href="<?php echo base_url(route_to('comercializacion')) ?>" class="text-secondary nav-link text-uppercase fw-semibold">Comercializacion</a></li>
                        <li class="p-3 px-md-0 nav-item py-md-1"><a href="<?php echo base_url(route_to('contacto')) ?>" class="text-secondary nav-link text-uppercase fw-semibold">Contacto</a></li>
                    </ul>

                    <ul class="navbar-nav flex-grow-1 justify-content-evenly">
                        <?php if(session()->is_logged): ?>
                            <li class="" ><?= session()->username  ?></li>
                            <li class="" ><a href="<?= base_url(route_to('detalle_compra_usuario'))?>">Compras</a></li>
                            <?php if(session()->user_group->name == 'admin'): ?>
                                <li class="p-3 px-md-0 nav-item py-md-1"><a href="<?php echo base_url(route_to('dashboard')) ?>" class="">Ir al Dash</a></li>
                            <?php endif;?>
                            <li class="p-3 px-md-0 nav-item py-md-1"><a href="<?php echo base_url(route_to('signout')) ?>" class="">Salir</a></li>
                        <?php else: ?>
                            <li class="p-3 px-md-0 nav-item py-md-1"><a href="<?php echo base_url(route_to('register'))?>" class="">Resgistrar</a></li>
                            <li class="p-3 px-md-0 nav-item py-md-1"><a href="<?php echo base_url(route_to('login')) ?>" class="">Login</a></li>
                        <?php endif; ?>
                    </ul>

                    <!-- <div class="flex-row mt-5 d-flex align-items-center mt-md-0 justify-content-center">
                        <a href="#"><i class="px-2 text-white bi bi-twitter fs-4"></i></a>
                        <a href="https://www.facebook.com/profile.php?id=61555808465997"><i class="px-2 text-white bi bi-facebook fs-4"></i></a>
                        <a href="#"><i class="px-2 text-white bi bi-whatsapp fs-4"></i></a>
                    </div> -->

                    
                </div>
            </section>
        </div>
    </nav>
</div>