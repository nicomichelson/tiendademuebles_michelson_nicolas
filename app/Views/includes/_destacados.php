<ul id="lista-cursos" class="list-unstyled mt-5 listado-productos">
    <?php foreach ($destacados as $v): ?>
        
        <li class="producto">
            <a href="<?= $v->getLinkTiendaProducto() ?>" class="text-decoration-none">
                <div class="card">
                    <img 
                        class="card-img-top"
                        
                        <?php if($v->getLink() != null): ?>
                            
                            src="<?= $v->getLink()?>"
                        <?php else: ?>
                            src="assets/img/producto<?= rand(1,4)?>.jpg"
                        <?php endif; ?>
                            alt="imagen producto"
                    />

                    <div class="card-body">
                        <h4 class="titulo"> <?= $v->name ?></h4>
                        <p class="descripcion-producto m-0"><?= $v->description ?></p>
                        <?php if(!empty($v->getCategorias())): ?>
                            <?php foreach($v->getCategorias() as $c):?>
                                <a href="#"> <?= $c->name ?> </a>   
                            <?php endforeach;?>    
                        <?php endif;?>
                        <p 
                            class="precio fw-bold"> 
                            <?php if($v->precio_vta):?> 
                                <span> <?= $v->precio_vta?></span>
                            <?php else: ?>
                                10 USD$
                            <?php endif; ?> 
                        </p>
                        <a href="#"  class="btnCompras agregar-carrito" data-id="<?= $v->id ?>">Agregar al Carrito</a>
                                
                    </div>
                </div>
            </a>
        </li>
    <?php endforeach ?>
</ul>