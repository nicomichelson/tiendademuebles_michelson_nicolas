<?php if(session('msg')): ?>
    <div class="alert alert-<?= session('msg.type') ?>" role="alert">
        <?=session('msg.body')?>
    </div>
<?php endif; ?>