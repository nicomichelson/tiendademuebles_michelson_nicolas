<footer class="bg-secondary py-5">
        <div class="container-xl">
            <div class="row align-items-start">
                <div class="col-md-2 d-grid gap-2">
                    <p class="mb-4 text-light">Categorías</p>
                    <nav class="d-flex flex-column">
                        <a class="text-light text-decoration-none" href="#">Oficina</a>
                        <a class="text-light text-decoration-none" href="#">Cocina</a>
                        <a class="text-light text-decoration-none" href="#">Jardín</a>
                        <a class="text-light text-decoration-none" href="#">Cochera</a>
                        <a class="text-light text-decoration-none" href="#">Dormitorios</a>
                    </nav>
                </div>
                

                <div class="col-md-2 d-grid gap-2">
                    <p class="mb-4 text-light">Sobre Nosotros</p>
                    <nav class="d-flex flex-column">
                        <a class="text-light text-decoration-none" href="nosotros">Nuestra Historia</a>
                        <a class="text-light text-decoration-none" href="#">Misión, Visión y Valores</a>
                        <a class="text-light text-decoration-none" href="terminos">Terminos y cond.</a>
                        <a class="text-light text-decoration-none" href="politicas_empresa">Política de la Empresa</a>
                        
                    </nav>
                </div>

                <div class="col-md-2 d-grid gap-2 me-auto">
                    <p class="mb-4 text-light">Soporte</p>
                    <nav class="d-flex flex-column">
                        <a class="text-light text-decoration-none" href="comercializacion">Comercializacion</a>
                        <a class="text-light text-decoration-none" href="#">Preguntas Frecuentes</a>
                        <a class="text-light text-decoration-none" href="#">Ayuda en Línea</a>
                        <a class="text-light text-decoration-none" href="<?php echo base_url(route_to('migration')) ?>">Migration</a>
                        <a class="text-light text-decoration-none" href="<?php echo base_url(route_to('seeder')) ?>">Seeders</a>
                    </nav>
                </div>

                <div class="col-md-2">
                    <a href="/contacto" class="btn btn-outline-light">Contacto</a>

                    <nav class="sociales">
                        <a class="sociales__enlace" href="https://facebook.com/carolinaspa">
                            <span class="sociales__accesible">Facebook</span>
                        </a>
                        <a class="sociales__enlace" href="https://twitter.com">
                            <span class="sociales__accesible">Twitter</span>
                        </a>
                        <a class="sociales__enlace" href="https://instagram.com">
                            <span class="sociales__accesible">Instagram</span>
                        </a>
                        <a class="sociales__enlace" href="https://youtube.com">
                            <span class="sociales__accesible">YouTube</span>
                        </a>
                        <a class="sociales__enlace" href="https://tiktok.com">
                            <span class="sociales__accesible">TikTok</span>
                        </a>
                    </nav>
                </div>
            </div>
        </div>

        <div class="container-xl mt-5 d-flex justify-content-between align-items-center">
            <p class="text-light fs-3 fw-bold"> <span class="text-success">Tienda de Muebles</span></p>

            <p class="text-light">&copy; Todos los derechos Reservados. TiendaMuebles</p>
        </div>
    </footer>