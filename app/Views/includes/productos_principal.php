<section class="productos container-xl mt-5 pt-5">
        <h3>Una amplia selección de Muebles para el Hogar y Oficina</h3>
        <p class="fs-4">Elige entre más de 155,000 productos en línea con nuevo contenido cada mes</p>

        <nav class="nav">
            <li class="nav-item">
                <a href="#" class="nav-link active fw-bold destacado">Destacados</a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link fw-bold oferta">Ofertas</a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link fw-bold masVendidos ">Mas Vendidos</a>
            </li>
        </nav>

        <div class="categoria-producto p-4 mt-3 bg-white">
            <p class="fs-4 fw-bold">Amplia tus oportunidades con Nuestros Productos</p>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Laborum molestias quis assumenda adipisci dignissimos enim natus dolor, ad aut quaerat omnis esse iure error velit cumque consequatur tempore. Nobis, rem?</p>

            <a href="#" class="btn btn-outline-dark">Explorar Mas Productos</a>

            <div class="productoOferta d-none ">
                <?= $this->include('includes/_ofertas') ?>
            </div>
            
            <div class="productoDestacado  ">
                <?= $this->include('includes/_destacados') ?>
            </div>
            
            <div class="productoMasVendido d-none">
                <?= $this->include('includes/_mas_vendidos') ?>
            </div>

        </div>
</section>