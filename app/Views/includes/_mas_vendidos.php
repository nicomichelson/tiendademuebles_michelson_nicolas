<ul class="list-unstyled mt-5 listado-productos">
    <li class="producto">
        <a href="#" class="text-decoration-none">
            <div class="card">
                <img 
                    class="card-img-top"
                    src="assets/img/producto1.jpg"
                    alt="imagen producto"
                />

                <div class="card-body">
                    <h4 class="titulo">Living Producto 1</h4>
                    <p class="descripcion-producto m-0">Producto 1</p>
                    
                    <p class="precio fw-bold">10 USD$ <span class="original fw-normal">200 USD$</span> </p>
                </div>
            </div>
        </a>
    </li>

    <li class="producto">
        <a href="#" class="text-decoration-none">
            <div class="card">
                <img 
                    class="card-img-top"
                    src="assets/img/producto2.jpg"
                    alt="imagen producto"
                />

                <div class="card-body">
                    <h4 class="titulo">Diseño de Living</h4>
                    <p class="descripcion-producto m-0">Producto2</p>
                
                    <p class="precio fw-bold">10 USD$ <span class="original fw-normal">200 USD$</span> </p>
                </div>
            </div>
        </a>
    </li>

  
</ul>