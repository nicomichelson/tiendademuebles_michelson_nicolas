<header class="dashboard__header">
    <div class="dashboard__header-grid d-flex flex-column flex-md-row justify-content-md-between">
        <a href="#">
            <h2 class="dashboard__logo">
                &#60;Dashboard
            </h2>
        </a>

        <nav class="dashboar__nav">
            <form action="<?php echo base_url(route_to('signout')) ?>" class="dashboard__form">
                <input type="submit" value="Cerrar Sesion" class="btn btn-primary">
            </form>
        </nav>

    </div>
</header>