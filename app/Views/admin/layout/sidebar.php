<aside class="dashboard__sidebar">
    <nav class="dashboard__menu">
        <a href="<?php echo base_url(route_to('dashboard')) ?>" class="dashboard__enlace">
            <i class="fa-solid fa-house dashboard__icono"></i>
            <span class="dashboard__menu-texto"> Inicio </span>
        </a>
        <a href="<?php echo base_url(route_to('productos')) ?>" class="dashboard__enlace">
            <i class="fa-solid fa-shop dashboard__icono"></i>
            <span class="dashboard__menu-texto"> Productos </span>
        </a>
        <a 
            
            href="<?php echo base_url(route_to('categories')) ?>" class="dashboard__enlace"
        
        >
            <i class="fa-solid fa-user dashboard__icono"></i>
            <span class="dashboard__menu-texto"> Categorias </span>
        </a>
        <a href="<?php echo base_url(route_to('ventas_cabecera')) ?>" class="dashboard__enlace">
            <i class="fa-solid fa-user dashboard__icono"></i>
            <span class="dashboard__menu-texto"> Ventas </span>
        </a>
        <a href="<?php echo base_url(route_to('users')) ?>" class="dashboard__enlace">
            <i class="fa-solid fa-user dashboard__icono"></i>
            <span class="dashboard__menu-texto"> Usuarios </span>
        </a>
    </nav>
</aside>
 