<?php echo $this->extend('admin_layout.php'); ?>

<?php echo $this->section('content'); ?>

<section class="container-xl">
        <div class="row justify-content-center py-5 my-5">
            <div class="col-md-8 bg-white rounded-5 shadow">
                <h2>Editar Categoria - <?= $category->name?></h2>
                <form class="p-5" action="<?php echo base_url(route_to('categories_update'))?>" method="POST">
                    <fieldset class="">

                        <div class="mb-3">
                            <label class="fs-6 form-label" for="nombre">Nombre:</label>
                            <input name="name" value="<?=old('name') ?? $category->name ?>" type="text" class="fs-6 form-control" id="nombre" placeholder="Tu Nombre" required>
                            <input name="id" value="<?=old('id') ?? $category->id ?>" type="hidden" class="fs-6 form-control" id="nombre" placeholder="Tu Nombre" required>
                            <span class="invalid-feedback"><?=session('errors.name')?></span>
                        </div>
                                               
                       

                    </fieldset>
                    <div class="d-grid d-md-flex">
                        <input class="btn btn-header" type="submit" value="Enviar Formulario">
                    </div>
                </form>
        
            </div>
        </div>
    </section>





<?php echo $this->endSection(); ?>

<?php echo $this->section('js'); ?>
    <!-- <script src="assets/js/mostrarProductos.min.js"></script> -->
<?php echo $this->endSection(); ?>