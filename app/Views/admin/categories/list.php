<?php echo $this->extend('admin_layout.php'); ?>

<?php echo $this->section('content'); ?>

   <h2>Categorias</h2>

   <section class="  py-md-5">
        <div class="container-xl">
        <a href="<?= base_url(route_to("categories_create"))?>">Agregar Categoria</a>
        <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($categories as $v): ?>
                        <tr>
                            <th scope="row"><?= $v->id ?></th>
                                
                                <td><?= $v->name ?></td>
                                <td><?= $v->description ?></td>
                                
                                <td>
                                    <a href="#">Ver/</a>
                                    <a href="<?= $v->getEditLink() ?>">Editar/</a>
                                    <a href="<?= $v->getDeleteLink() ?>">Eliminar</a>
                                </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>

            <div class="d-flex justify-content-center">
                <?= $pager->links() ?>
            </div>
        </div>

        
    </section>

<?php echo $this->endSection(); ?>

<?php echo $this->section('js'); ?>
    <!-- <script src="assets/js/mostrarProductos.min.js"></script> -->
<?php echo $this->endSection(); ?>