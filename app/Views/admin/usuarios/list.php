<?php echo $this->extend('admin_layout.php'); ?>

<?php echo $this->section('content'); ?>

   <h2>Usuarios</h2>

   <section class="  py-md-5">
        <div class="container-xl">
        <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Username</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Email</th>
                        <th scope="col">Grupo</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $v): ?>
                        <tr>
                            <th scope="row"><?= $v->id ?></th>
                                <td><?= $v->username ?></td>
                                <td><?= $v->name ?></td>
                                <td><?= $v->surname ?></td>
                                <td><?= $v->email ?></td>
                                <td><?= $v->group ?></td>
                                <td>
                                    <a href="#">Ver/</a>
                                    <a href="#">Actualizar/</a>
                                    <a href="#">Eliminar</a>
                                </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>

            <div class="d-flex justify-content-center">
                <?= $pager->links() ?>
            </div>
        </div>

        
    </section>

<?php echo $this->endSection(); ?>

<?php echo $this->section('js'); ?>
    <!-- <script src="assets/js/mostrarProductos.min.js"></script> -->
<?php echo $this->endSection(); ?>