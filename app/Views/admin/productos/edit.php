<?php echo $this->extend('admin_layout.php'); ?>

<?php echo $this->section('content'); ?>

<section class="container-xl">
        <div class="row justify-content-center py-5 my-5">
            <div class="col-md-8 bg-white rounded-5 shadow">
                
                <form class="p-5" enctype="multipart/form-data" action="<?php echo base_url(route_to('products_update'))?>" method="POST">
                    <input type="hidden" name="_method" value="put">
                    <input type="hidden" name="id" value="<?= $product->id ?>">
                    <fieldset class="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="fs-6 form-label" for="name">Nombre:</label>
                                    <input name="name" value="<?=old('name') ?? $product->name ?>" type="text" class="fs-6 form-control" id="nombre" placeholder="Tu Nombre" required>
                                    <span class="text-danger"><?=session('errors.name')?></span>
                                </div>
                                                    
                                <div class="mb-3">
                                    <label class="fs-6 form-label" for="description">Descripcion:</label>
                                    <textarea name="description" value="<?=old('description') ?? $product->description ?>" type="text" class="fs-6 form-control" id="nombre" placeholder="" required></textarea>
                                    <span class="text-danger"><?=session('errors.description')?></span>
                                </div>
                                <div class="mb-3">
                                    <label class="fs-6 form-label" for="precio">Precio:</label>
                                    <input min="0" step="0.01" name="precio" value="<?=old('precio') ?? $product->precio ?>" type="number" class="fs-6 form-control" id="precio" placeholder="" required>
                                    <span class="text-danger"><?=session('errors.precio')?></span>
                                </div>
                                <div class="mb-3">
                                    <label class="fs-6 form-label" for="precio_vta">Precio de Venta:</label>
                                    <input min="0" step="0.01" name="precio_vta" value="<?=old('precio_vta') ?? $product->precio_vta ?>" type="number" class="fs-6 form-control" id="precio_vta" placeholder="" required>
                                    <span class="text-danger"><?=session('errors.precio_vta')?></span>
                                </div>   
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="stock" class="form-label">Stock</label>
                                    <input name="stock" class="form-control" value="<?=old('stock') ?? $product->stock ?>" type="number" id="stock">
                                </div>
                                <div class="mb-3">
                                    <label for="stock_min" class="form-label">Stock Minimo</label>
                                    <input name="stock_min" class="form-control" value="<?=old('stock_min') ?? $product->stock_min ?>" type="number" id="stock_min">
                                </div>
                                <div class="mb-3">
                                    <label for="file" class="form-label">Subir Imagen</label>
                                    <input value="<?=old('file') ?? $product->file ?>" name="file" class="form-control" type="file" id="formFile">
                                </div>
                                <div class="mb-3">
                                    <label class="fs-6 form-label" for="categories">Categorias:</label>
                                    <?php if(empty($categories)): ?>
                                        <a href="<?php base_url(route_to('categories_create'))?>">agregar una categoria</a>
                                    <?php else: ?>
                                        <?php foreach ($categories as $v): ?>                             
                                        
                                        <input name="categories[]" value="<?= $v->id ?>" class="form-check-input" type="checkbox"
                                            <?=
                                                old('categories.*')
                                                ?
                                                    (in_array($v->id , old('categories.*'))
                                                        ? 'checked'
                                                        : '')
                                                : ''   
                                            ?>
                                            
                                        />
                                        <label class=""><?= $v->name ?></label>
                                        <?php endforeach; ?>
                                        <span class="invalid-feedback"><?=session('errors')['categories.*'] ?? '' ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        

                    </fieldset>
                    <div class="d-grid d-md-flex">
                        <input class="btn btn-header" type="submit" value="Enviar Formulario">
                    </div>
                </form>
        
            </div>
        </div>
    </section>





<?php echo $this->endSection(); ?>

<?php echo $this->section('js'); ?>
    <!-- <script src="assets/js/mostrarProductos.min.js"></script> -->
<?php echo $this->endSection(); ?>