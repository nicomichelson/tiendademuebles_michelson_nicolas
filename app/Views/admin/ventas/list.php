<?php echo $this->extend('admin_layout.php'); ?>

<?php echo $this->section('content'); ?>

   <h2>Ventas de Productos</h2>

    <section class="  py-md-5">
        <div class="container-xl">
        
        <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Venta N.</th>
                        <th scope="col">Usuario Comprador</th>
                        <th scope="col">Total de la Venta</th>
                        <th scope="col">Fecha de compra</th>
                        <th scope="col">Cantidad de articulos</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($ventas as $v): ?>
                        <tr>
                            <th scope="row"><?= $v->id ?></th>
                                <td><?= $v->getUser() ?></td>
                                
                                <td><?= $v->total_venta ?>USD </td>
                                <td><?= $v->created_at ?> </td>
                                <td><?= $v->cantidad ?></td>
                                <td>
                                    <a href="<?= $v->getLinkDetallesCompra()?>">Detalles de compra/</a>
                                 
                                </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>

            <div class="d-flex justify-content-center">
                <?= $pager->links() ?>
            </div>
        </div>

        
    </section>

<?php echo $this->endSection(); ?>

<?php echo $this->section('js'); ?>
    <!-- <script src="assets/js/mostrarProductos.min.js"></script> -->
<?php echo $this->endSection(); ?>