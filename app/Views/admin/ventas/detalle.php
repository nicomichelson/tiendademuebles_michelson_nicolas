<?php echo $this->extend('admin_layout.php'); ?>

<?php echo $this->section('content'); ?>

   <h2>Detalles de Venta</h2>

   <section class="  py-md-5">
        <div class="container-xl">
        <a href="<?= route_to('ventas_cabecera')?>">Volever A Lista de Ventas</a>
        <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Imagen</th>
                        <th scope="col">Stock</th>
                        <th scope="col">Precio Unitario</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($products as $v): ?>
                        <tr>
                            <th scope="row"><?= $v->id ?></th>
                                <td><?= $v->name ?></td>
                                <td>

                                <img 
                                    class="dash-img"
                                    
                                    <?php if($v->getLink() != null): ?>
                                        
                                        src="<?= $v->getLink()?>"
                                    <?php else: ?>
                                        src="<?= base_url('assets/img/producto').rand(1,4)?>.jpg"
                                    <?php endif; ?>
                                        alt="imagen producto"
                                />

                                </td>
                                <td><?= $v->stock ?></td>
                                <td><?= $v->precio ?> USD</td>
                                <td>
                                    <a href="<?= $v->getShowLink()?>">Ver Producto/</a>
                                    
                                </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>

            <div class="d-flex justify-content-center">
                <?= $pager->links() ?>
            </div>
        </div>

        
    </section>

<?php echo $this->endSection(); ?>

<?php echo $this->section('js'); ?>
    <!-- <script src="assets/js/mostrarProductos.min.js"></script> -->
<?php echo $this->endSection(); ?>