<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@300;400;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
   
    <link rel="stylesheet" href='<?php echo base_url("assets/css/app.css")?>' />

</head>
<body class="dashboard container-fluid d-flex flex-column">
    <?= $this->include('admin/layout/header') ?>
    <section class="dashboard__grid ">
        <?= $this->include('admin/layout/sidebar') ?>
        <main class="dashboard__contenido">
            
            <?php echo $this->renderSection('content'); ?>
        </main>
    </section>
    
    <!-- <script src="assets/js/boostrap/js/bootstrap.bundle.min.js"></script> -->
</body>
</html>