document.addEventListener('DOMContentLoaded', function(){
    eventListener();
});



function eventListener(){
    const oferta = document.querySelector('.oferta');
    const destacado = document.querySelector('.destacado');
    const masVendidos = document.querySelector('.masVendidos');

    destacado.addEventListener('click', mostrarDestacado);
    oferta.addEventListener('click', mostrarOferta);
    masVendidos.addEventListener('click', mostrarMasVendidos);
}

function mostrarDestacado(e){
    e.preventDefault();
    const productoDestacado = document.querySelector('.productoDestacado');
    const productoOferta = document.querySelector('.productoOferta');
    const productoMasVendido = document.querySelector('.productoMasVendido');
    
    if(productoDestacado.classList.contains('d-none')){
        productoDestacado.classList.remove('d-none');
        productoOferta.classList.add('d-none');
        productoMasVendido.classList.add('d-none');
    }

}

function mostrarOferta(e){
    e.preventDefault();
    const productoDestacado = document.querySelector('.productoDestacado');
    const productoOferta = document.querySelector('.productoOferta');
    const productoMasVendido = document.querySelector('.productoMasVendido');
    
    if(productoOferta.classList.contains('d-none')){
        productoDestacado.classList.add('d-none');
        productoOferta.classList.remove('d-none');
        productoMasVendido.classList.add('d-none');
    }
}

function mostrarMasVendidos(e){
    e.preventDefault();
    const productoDestacado = document.querySelector('.productoDestacado');
    const productoOferta = document.querySelector('.productoOferta');
    const productoMasVendido = document.querySelector('.productoMasVendido');
    
    if(productoMasVendido.classList.contains('d-none')){
        productoDestacado.classList.add('d-none');
        productoOferta.classList.add('d-none');
        productoMasVendido.classList.remove('d-none');
    }
}